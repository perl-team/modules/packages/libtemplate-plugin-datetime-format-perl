Source: libtemplate-plugin-datetime-format-perl
Section: perl
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Julien Vaubourg <julien@vaubourg.com>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: perl,
                     libclass-load-perl,
                     libdatetime-perl,
                     libtemplate-perl,
                     libtest-exception-perl,
                     libtest-pod-coverage-perl,
                     libtest-pod-perl,
                     libtest-simple-perl | libtest-use-ok-perl | perl
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtemplate-plugin-datetime-format-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtemplate-plugin-datetime-format-perl.git
Homepage: https://metacpan.org/release/Template-Plugin-DateTime-Format
Testsuite: autopkgtest-pkg-perl

Package: libtemplate-plugin-datetime-format-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends},
         libclass-load-perl,
         libdatetime-perl
Description: module for formatting DateTime objects from TT with DateTime::Format
 Oftentimes, you have a DateTime object that you want to render in
 your template. However, the default rendering (2008-01-01T01:23:45)
 is pretty ugly. Formatting the DateTime with a DateTime::Format object
 is the usual solution, but there's usually not a nice place to put the
 formatting code.
 .
 Template::Plugin::Datetime::Format solves that problem. You can create
 a formatter object from within TT and then use that object to format
 DateTime objects.
